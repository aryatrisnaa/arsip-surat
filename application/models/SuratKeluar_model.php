<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class SuratKeluar_model extends CI_Model
{

  public function view()
  {
    return $this->db->query('SELECT * from SuratKeluar')->result();
  }

  public function save($data)
  {
    $this->db->insert('SuratKeluar', $data);
  }

  public function update($where, $data)
  {
    $this->db->where($where);
    $this->db->update('SuratKeluar', $data);
  }

  public function download($id){
		$query = $this->db->get_where('SuratKeluar',array('id'=>$id));
		return $query->row_array();
	}

  public function delete($id)
  {
    return $this->db->delete('SuratKeluar', array("id" => $id));
  }

  public function all_SuratKeluar_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('SuratKeluar');

    return $query->num_rows();
  }

  public function all_SuratKeluar_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('SuratKeluar');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_SuratKeluar_count($search)
  {
    $query = $this
      ->db
      ->like('judul', $search)
      ->or_like('no_surat', $search)
      ->or_like('keluar', $search)
      ->or_like('tgl_surat', $search)
      ->or_like('judul', $search)
      ->or_like('keterangan', $search)
      ->where('deleted_at',null)
      ->get('SuratKeluar');

    return $query->num_rows();
  }

  public function search_SuratKeluar_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('judul', $search)
      ->or_like('no_surat', $search)
      ->or_like('keluar', $search)
      ->or_like('tgl_surat', $search)
      ->or_like('judul', $search)
      ->or_like('keterangan', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->where('deleted_at',null)
      ->get('SuratKeluar');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  
}
