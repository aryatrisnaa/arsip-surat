<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class SuratMasuk_model extends CI_Model
{

  public function view()
  {
    return $this->db->query('SELECT * from SuratMasuk')->result();
  }

  public function save($data)
  {
    $this->db->insert('SuratMasuk', $data);
  }

  public function saveDispo($data)
  {
    $this->db->insert('SuratDispo', $data);
  }

  public function update($where, $data)
  {
    $this->db->where($where);
    $this->db->update('SuratMasuk', $data);
  }

  public function download($id){
		$query = $this->db->get_where('SuratMasuk',array('id'=>$id));
		return $query->row_array();
  }
  
  public function download_dispo($id){
		$query = $this->db->get_where('SuratDispo',array('id_dispo'=>$id));
		return $query->row_array();
	}

  public function delete($id)
  {
    return $this->db->delete('SuratMasuk', array("id" => $id));
  }

  public function all_SuratMasuk_count()
  {
    $query = $this
      ->db
      ->where('deleted_at',null)
      ->get('SuratMasuk');

    return $query->num_rows();
  }
  public function surat_terdispo_count()
  {
    $query = $this
      ->db
      ->group_by('SuratMasuk.no_surat')
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.no_surat IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.no_surat')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function surat_terdispo_anda_count()
  {
    $akun = $this->session->userdata('nama');
    $query = $this
      ->db
      ->group_by('SuratMasuk.no_surat')
      ->like('SuratDispo.kepada', $akun)
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.no_surat IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.no_surat')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function surat_terdispo_anda_ditanggapi()
  {
    $akun = $this->session->userdata('nama');
    $query = $this
      ->db
      ->group_by('SuratMasuk.no_surat')
      ->like('SuratDispo.kepada', $akun)
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo IS NOT NULL',null, false)
      ->where('SuratDispo.no_surat IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.no_surat')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function surat_terdispo_anda_blmtanggap()
  {
    $akun = $this->session->userdata('nama');
    $query = $this
      ->db
      ->group_by('SuratMasuk.no_surat')
      ->like('SuratDispo.kepada', $akun)
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo',null)
      ->where('SuratDispo.no_surat IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.no_surat')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function surat_terdispo_blmtanggap()
  {
    $query = $this
      ->db
      ->group_by('SuratMasuk.no_surat')
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo',null)
      ->where('SuratDispo.no_surat IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.no_surat')
      ->get('SuratMasuk');

    return $query->num_rows();
  }
  public function surat_terdispo_blmtanggap_data()
  {
    $query = $this
      ->db
      ->order_by('SuratMasuk.created_at', 'DESC')
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.id')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratDispo.kepada')
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo',null)
      ->where('SuratDispo.no_surat IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.no_surat, SuratMasuk.created_at, SuratMasuk.id, SuratDispo.kepada')
      ->get('SuratMasuk');

      if ($query->num_rows() > 0) {
        return $query->result();
      } else {
        return null;
      }
  }

  public function all_SuratMasuk_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.id')
      ->group_by('SuratMasuk.tgl_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.keterangan')
      ->group_by('SuratMasuk.nama_file')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratMasuk.created_by')
      ->group_by('SuratMasuk.deleted_at')
      ->group_by('SuratMasuk.deleted_by')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.status_dispo')
      ->group_by('SuratKeluar.balasan_surat')
      ->where('SuratMasuk.deleted_at',null)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.*, SuratDispo.no_surat as dispo, SuratDispo.status_dispo as status, SuratKeluar.balasan_surat as balasan')
      ->get('SuratMasuk');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function search_SuratMasuk_count($search)
  {
    $query = $this
      ->db
      ->like('SuratMasuk.judul', $search)
      ->or_like('SuratMasuk.no_surat', $search)
      ->or_like('SuratMasuk.dari', $search)
      ->or_like('SuratMasuk.tgl_surat', $search)
      ->or_like('SuratMasuk.judul', $search)
      ->or_like('SuratMasuk.keterangan', $search)
      ->group_by('SuratMasuk.no_surat')
      ->where('SuratMasuk.deleted_at',null)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.no_surat ')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function search_SuratMasuk_data($limit, $start, $col, $dir, $search)
  {
    $query = $this
      ->db
      ->like('SuratMasuk.judul', $search)
      ->or_like('SuratMasuk.no_surat', $search)
      ->or_like('SuratMasuk.dari', $search)
      ->or_like('SuratMasuk.tgl_surat', $search)
      ->or_like('SuratMasuk.judul', $search)
      ->or_like('SuratMasuk.keterangan', $search)
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.id')
      ->group_by('SuratMasuk.tgl_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.keterangan')
      ->group_by('SuratMasuk.nama_file')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratMasuk.created_by')
      ->group_by('SuratMasuk.deleted_at')
      ->group_by('SuratMasuk.deleted_by')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.status_dispo')
      ->group_by('SuratKeluar.balasan_surat')
      ->where('SuratMasuk.deleted_at',null)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->join('SuratKeluar','SuratKeluar.balasan_surat = SuratMasuk.no_surat','left')
      ->select('SuratMasuk.*, SuratDispo.no_surat as dispo, SuratDispo.status_dispo as status, SuratKeluar.balasan_surat as balasan')
      ->get('SuratMasuk');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  
}
