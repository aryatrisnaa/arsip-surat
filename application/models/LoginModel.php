
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model
{
    function check_session()
    {
        return $this->session->userdata('user');
    }

    function check_user($user,$pass)
    {
        $this->db->select('*');
        $this->db->from('Akun');
        $this->db->where('username',$user);   
        $this->db->where('pass',$pass);
        $this->db->limit(1);
        $query = $this->db->get();
        
        return $query;
    }

}