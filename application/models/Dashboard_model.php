<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{  
  
  public function all_pencarian_count($cari)
  {
    $sql = "SELECT * FROM (SELECT id, tgl_surat, no_surat, judul, dari as objek , keterangan, deleted_at FROM SuratMasuk where deleted_at is null
    UNION SELECT id, tgl_surat, no_surat, judul, keluar as objek, keterangan, deleted_at FROM SuratKeluar where deleted_at is null) as gabungan 
    where gabungan.tgl_surat like '%$cari%' or  gabungan.no_surat like '%$cari%' or gabungan.judul like '%$cari%' or gabungan.objek like '%$cari%' or gabungan.keterangan like '%$cari%'";
    
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function all_pencarian_data($limit, $start, $col, $dir, $cari)
  {
    $sql = "SELECT TOP $limit * FROM (SELECT id, tgl_surat, no_surat, judul, dari as objek , keterangan, created_at, deleted_at FROM SuratMasuk where deleted_at is null
    UNION SELECT id, tgl_surat, no_surat, judul, keluar as objek, keterangan, created_at, deleted_at FROM SuratKeluar where deleted_at is null) as gabungan 
    where gabungan.tgl_surat like '%$cari%' or  gabungan.no_surat like '%$cari%' or gabungan.judul like '%$cari%' or gabungan.objek like '%$cari%' or gabungan.keterangan like '%$cari%'";
    
    $sql .= " ORDER BY ".$col." ".$dir." ";
        
    $query = $this->db->query($sql);
    
    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function all_tanggapan_count()
  {
    $query = $this
      ->db
      ->group_by('SuratMasuk.id')
      ->group_by('SuratDispo.tgl_ditanggapi')
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratDispo.isi')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.kepada')
      ->group_by('SuratDispo.tanggapan')
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->select('SuratDispo.tgl_ditanggapi, SuratMasuk.created_at, SuratMasuk.id, SuratMasuk.no_surat as no_surat, SuratMasuk.judul, SuratMasuk.dari, SuratDispo.isi, SuratDispo.no_surat as dispo, SuratDispo.kepada, SuratDispo.tanggapan')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function all_tanggapan_data($limit, $start, $col, $dir)
  {
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->group_by('SuratMasuk.id')
      ->group_by('SuratDispo.tgl_ditanggapi')
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratDispo.isi')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.kepada')
      ->group_by('SuratDispo.tanggapan')
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->select('SuratDispo.tgl_ditanggapi, SuratMasuk.created_at, SuratMasuk.id, SuratMasuk.no_surat as no_surat, SuratMasuk.judul, SuratMasuk.dari, SuratDispo.isi, SuratDispo.no_surat as dispo, SuratDispo.kepada, SuratDispo.tanggapan')
      ->get('SuratMasuk');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function all_tanggapan_sudah_count()
  {
    $akun = $this->session->userdata('nama');
    $query = $this
      ->db
      ->group_by('SuratMasuk.id')
      ->group_by('SuratDispo.tgl_ditanggapi')
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratDispo.isi')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.kepada')
      ->group_by('SuratDispo.tanggapan')
      ->like('SuratDispo.kepada', $akun)
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->select('SuratDispo.tgl_ditanggapi, SuratMasuk.created_at, SuratMasuk.id, SuratMasuk.no_surat as no_surat, SuratMasuk.judul, SuratMasuk.dari, SuratDispo.isi, SuratDispo.no_surat as dispo, SuratDispo.kepada, SuratDispo.tanggapan')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function all_tanggapan_sudah_data($limit, $start, $col, $dir)
  {
    $akun = $this->session->userdata('nama');
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->group_by('SuratMasuk.id')
      ->group_by('SuratDispo.tgl_ditanggapi')
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratDispo.isi')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.kepada')
      ->group_by('SuratDispo.tanggapan')
      ->like('SuratDispo.kepada', $akun)
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo IS NOT NULL',null, false)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->select('SuratDispo.tgl_ditanggapi, SuratMasuk.created_at, SuratMasuk.id, SuratMasuk.no_surat as no_surat, SuratMasuk.judul, SuratMasuk.dari, SuratDispo.isi, SuratDispo.no_surat as dispo, SuratDispo.kepada, SuratDispo.tanggapan')
      ->get('SuratMasuk');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }

  public function all_tanggapan_belum_count()
  {
    $akun = $this->session->userdata('nama');
    $query = $this
      ->db
      ->group_by('SuratMasuk.id')
      ->group_by('SuratDispo.tgl_ditanggapi')
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratDispo.isi')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.kepada')
      ->group_by('SuratDispo.tanggapan')
      ->like('SuratDispo.kepada', $akun)
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo',null)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->select('SuratDispo.tgl_ditanggapi, SuratMasuk.created_at, SuratMasuk.id, SuratMasuk.no_surat as no_surat, SuratMasuk.judul, SuratMasuk.dari, SuratDispo.isi, SuratDispo.no_surat as dispo, SuratDispo.kepada, SuratDispo.tanggapan')
      ->get('SuratMasuk');

    return $query->num_rows();
  }

  public function all_tanggapan_belum_data($limit, $start, $col, $dir)
  {
    $akun = $this->session->userdata('nama');
    $query = $this
      ->db
      ->limit($limit, $start)
      ->order_by($col, $dir)
      ->group_by('SuratMasuk.id')
      ->group_by('SuratDispo.tgl_ditanggapi')
      ->group_by('SuratMasuk.no_surat')
      ->group_by('SuratMasuk.judul')
      ->group_by('SuratMasuk.dari')
      ->group_by('SuratMasuk.created_at')
      ->group_by('SuratDispo.isi')
      ->group_by('SuratDispo.no_surat')
      ->group_by('SuratDispo.kepada')
      ->group_by('SuratDispo.tanggapan')
      ->like('SuratDispo.kepada', $akun)
      ->where('SuratMasuk.deleted_at',null)
      ->where('SuratDispo.status_dispo',null)
      ->join('SuratDispo','SuratDispo.no_surat = SuratMasuk.no_surat','left')
      ->select('SuratDispo.tgl_ditanggapi, SuratMasuk.created_at, SuratMasuk.id, SuratMasuk.no_surat as no_surat, SuratMasuk.judul, SuratMasuk.dari, SuratDispo.isi, SuratDispo.no_surat as dispo, SuratDispo.kepada, SuratDispo.tanggapan')
      ->get('SuratMasuk');

    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }
  
}
