<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
</head>

<body class="">
  <div class="wrapper ">
    <?php $this->load->view("partials/sidebar.php") ?>
    <div class="main-panel">
    <?php $this->load->view("partials/navbar.php") ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><b>Daftar Surat Keluar</b></h4>
                  <?php
                    if($this->session->flashdata('success')){
                      ?>
                      <div class="alert alert-success text-center" style="margin-top:20px;">
                        <?php echo $this->session->flashdata('success'); ?>
                      </div>
                      <?php
                    }

                    if($this->session->flashdata('error')){
                      ?>
                      <div class="alert alert-danger text-center" style="margin-top:20px;">
                        <?php echo $this->session->flashdata('error'); ?>
                      </div>
                      <?php
                    }
                    ?>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <div class="pull-right"><a class="btn btn-success btn-sm" style="color:#fff" data-toggle="modal" data-target="#modal_keluar_add_new"><span class="material-icons">add_task</span> Tambah Data</a></div>
                    <br><br>
                    <table class="table table-bordered table-striped" id="keluar-table" width="100%" cellspacing="0">
                      <thead>
                        <tr class="info">
                          <th style="text-align:center">No</th>
                          <th style="text-align:center">Tanggal Surat</th>
                          <th style="text-align:center">No Surat</th>
                          <th style="text-align:center">Perihal</th>
                          <th style="text-align:center">Ke</th>
                          <th style="text-align:center">Tanggal Upload</th>
                          <th style="text-align:center">Keterangan</th>
                          <th style="text-align:center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- footer -->
      <!-- </?php $this->load->view("partials/footer.php") ?> -->
    </div>
  </div>

  <!-- ============ MODAL ADD SURAT KELUAR =============== -->
  <div class="modal fade" id="modal_keluar_add_new" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <center><h3 >Tambah Data Surat Keluar</h3></center>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>SuratKeluar/simpan_keluar" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="">
                <label class="col-md-3">Tanggal Surat :</label><br>
                <div class="col-md-12">
                  <input type="text" class="form-control input-tanggal" name="surat_tanggal_keluar" id="surat_tanggal_keluar" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">No Surat :</label><br>
                <div class="col-md-12">
                  <input name="no_surat_keluar" id="no_surat_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Perihal :</label><br>
                <div class="col-md-12">
                  <input name="surat_judul_keluar" id="surat_judul_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Kepada :</label><br>
                <div class="col-md-12">
                  <input name="surat_keluar" id="surat_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Keterangan :</label><br>
                <div class="col-md-12">
                  <textarea class="form-control" id="surat_keterangan_keluar" name="surat_keterangan_keluar" placeholder=""></textarea>
                </div>
              </div><br>

              <div class="col-md-12">
                <label>File:</label><br>
                <input type="file" name="upload_keluar" id="upload_keluar" required>
                <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : pdf</p>
              </div><br>
            </div>
        </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-info">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL ADD SURAT KELUAR-->

  <!-- ============ MODAL edit SURAT KELUAR =============== -->
  <div class="modal fade" id="modal_keluar_edit" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <center><h3 >Edit Data Surat Keluar</h3></center>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>SuratKeluar/update_data" enctype="multipart/form-data">
            <div class="modal-body">

              <input type="hidden" id="edit_surat_id_keluar" class="form-control hidden" name="edit_surat_id_keluar">

              <div class="">
                <label class="col-md-3">Tanggal Surat :</label><br>
                <div class="col-md-12">
                  <input type="text" class="form-control input-tanggal" name="edit_surat_tanggal_keluar" id="edit_surat_tanggal_keluar" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">No Surat :</label><br>
                <div class="col-md-12">
                  <input name="edit_no_surat_keluar" id="edit_no_surat_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Perihal :</label><br>
                <div class="col-md-12">
                  <input name="edit_surat_judul_keluar" id="edit_surat_judul_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Kepada :</label><br>
                <div class="col-md-12">
                  <input name="edit_surat_keluar" id="edit_surat_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Keterangan :</label><br>
                <div class="col-md-12">
                  <textarea class="form-control" id="edit_surat_keterangan_keluar" name="edit_surat_keterangan_keluar" placeholder="max. 255 karakter"></textarea>
                </div>
              </div><br>
            </div>
        </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-info">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL edit SURAT KELUAR-->
  
  <!-- JS -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>

  <script type="text/javascript">
    $(document).ready(function() {

      $('.input-tanggal').daterangepicker({
        format          : "dd-mm-yyyy",
        singleDatePicker: true,
        autoApply       : true,
        todayHighlight  : true,
        locale: {
          format: "DD-MM-YYYY",
          // separator: " - ",
        }
      });
      
      var table = $('#keluar-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "SuratKeluar/data_keluar",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "tgl_surat", 'sortable': false},
          { "data": "no_surat", "className": "text-center", 'sortable': false},
          { "data": "judul", 'sortable': false},
          { "data": "keluar", 'sortable': false},
          { "data": "created_at", 'sortable': false},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function (row, data, index) {
					var info = table.page.info();
					var value = index + 1 + info.start;
					$('td', row).eq(0).html(value);
				}
      });
      
      $('#keluar-table_filter input').unbind();
      $('#keluar-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#keluar-table').DataTable().ajax.reload();
      });

    });

    // EDIT data
    function edit_data(id){
        $.ajax({
          type: "get",
          url: 'SuratKeluar/edit_data/'+id,
        })
        .done(function (response) {
            var result = JSON.parse(response)
            
            $('#edit_surat_id_keluar').val(result.query.id);
            $('#edit_surat_tanggal_keluar').val(result.query.tgl_surat);
            $('#edit_no_surat_keluar').val(result.query.no_surat);
            $('#edit_surat_judul_keluar').val(result.query.judul);
            $('#edit_surat_keluar').val(result.query.keluar);
            $('#edit_surat_keterangan_keluar').val(result.query.keterangan);
            
            $("#modal_keluar_edit").modal('show');
            
        });
      }

    function delete_data(id){
        var url = '<?php echo site_url('SuratKeluar/delete_data/') ?>'+id;
        // console.log(url);
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
      }
      function downloadfile(id){
        window.location = '<?=base_url();?>'+'SuratKeluar/download/'+id;
      }

      function detail_data(id){
        window.location = '<?=base_url();?>'+'SuratKeluar/detail_data/'+id;
      }
</script>
<script>
    ClassicEditor
        .create( document.querySelector( '#surat_keterangan_keluar' ) )
        .catch( error => {
            console.error( error );
        } );
	ClassicEditor
        .create( document.querySelector( '#edit_surat_keterangan_keluar' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
</body>

</html>