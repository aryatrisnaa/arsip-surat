<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
</head>

<body class="">
  <div class="wrapper ">
    <?php $this->load->view("partials/sidebar.php") ?>
    <div class="main-panel">
    <?php $this->load->view("partials/navbar.php") ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><b>Detail Surat Keluar</b></h4>
                </div>
                <div class="card-body"><br>
                  <div class="row">
                    
                    <?php foreach ($detail as $s): ?>
                    <div class="col-lg-4">
                        <table class="table table-bordered">
                            <tr>
                                <th width="39%">Tanggal Surat</th>
                                <td><a><?php echo $s->tgl_surat ?></a></td>            
                            </tr> 
                            <tr>
                                <th>No. Surat</th>
                                <td><a><?php echo $s->no_surat ?></a></td>            
                            </tr>
                            <tr>
                                <th>Perihal</th>
                                <td><a><?php echo $s->judul ?></a></td>            
                            </tr>
                            <tr>
                                <th>Kepada</th>
                                <td><a><?php echo $s->keluar ?></a></td>            
                            </tr>           
                            <tr>
                                <th>Keterangan</th>
                                <td><a><?php echo $s->keterangan ?></a></td>            
                            </tr>
                            <tr>
                                <th>Tanggal Upload</th>
                                <td><a><?php echo substr($s->created_at,0,19) ?></a></td>            
                            </tr>
                        </table>
                        <br>
                          <?php if($s->id_masuk != NULL) { ?>
                            <table class="table table-bordered">
                              <tr>
                                  <th width="39%">Balasan dari Surat </th>
                                  <td><center><a class="btn btn-success btn-sm" href="<?=base_url('SuratMasuk/detail_data/'.$s->id_masuk)?>"><span class="material-icons">assignment</span> Lihat Surat</a></center></td>            
                              </tr> 
                            </table>
                          <?php } else {
                            }?> 
                    </div>

                    <div class="col-md-8 row justify-content-center">
                      <table class="table table-responsive">
                        <tr>
                          <embed type="application/pdf" src="<?php echo base_url('upload/keluar/'.$s->nama_file); ?>" width="600" height="400"></embed>              
                        </tr>
                      </table>
                    </div>
                    <?php endforeach; ?>    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- footer -->
      <!-- </?php $this->load->view("partials/footer.php") ?> -->
    </div>
  </div>
  
  <!-- JS -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>

  <script>
    $('#dataTable').DataTable({
        "ordering": true
    });
</script>
</body>

</html>