<!-- Button trigger modal -->
<button id="alert_warning_login" type="button" class="btn btn-primary hidden" style="visibility: hidden" data-toggle="modal" data-target="#warningLogin">
  Warning!
</button>

<!-- Modal -->
<div class="modal fade bd-example-modal-sm" id="warningLogin" tabindex="-1" role="dialog" aria-labelledby="warningLoginLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #cf4a40;">
        <!-- <h2 class="modal-title" id="warningLoginLabel">Warning!</h2> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
       Username dan password tidak valid!
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- ==================================================MODAL AKUN PROFIL=========================================================================================== -->

<button id="alert_profil_akun" type="button" class="btn btn-primary hidden" style="visibility: hidden" data-toggle="modal" data-target="#warningProfil">
  Warning!
</button>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="warningProfil" tabindex="-1" role="dialog" aria-labelledby="warningProfilLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #cf4a40;">
        <!-- <h2 class="modal-title" id="warningLoginLabel">Warning!</h2> -->
        
      </div>
      <div class="modal-body text-center">
       Silahkan logout dan login menggunakan akun <br><b>profil SIM Aset!</b>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-light" onclick="return logout()" href="javascript:void(0)">Logout</button>
      </div>
    </div>
  </div>
</div>

<!-- ==================================================MODAL AKUN SIMASET=========================================================================================== -->

<button id="alert_simaset" type="button" class="btn btn-primary hidden" style="visibility: hidden" data-toggle="modal" data-target="#warningsimaset">
  Warning!
</button>

<!-- Modal -->
<div class="modal fade bd-example-modal-lg" id="warningsimaset" tabindex="-1" role="dialog" aria-labelledby="warningsimasetLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #cf4a40;">
        <!-- <h2 class="modal-title" id="warningLoginLabel">Warning!</h2> -->
        
      </div>
      <div class="modal-body text-center">
       Silahkan logout dan login menggunakan akun <br><b>SIM Aset</b> Anda!
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-light" onclick="return logout()" href="javascript:void(0)">Logout</button>
      </div>
    </div>
  </div>
</div>

<!-- ==================================================MODAL AKUN UPLOAD=========================================================================================== -->

<button id="alert_denied_login" type="button" class="btn btn-primary hidden" style="visibility: hidden" data-toggle="modal" data-target="#deniedLogin">
  Warning!
</button>

<!-- Modal -->
<div class="modal fade bd-example-modal-sm" id="deniedLogin" role="dialog" aria-labelledby="warningLoginLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #cf4a40;">
        <h4 class="modal-title" style="color:#fff">Ditolak!</h4>

      </div>
      <div class="modal-body text-center">
       Akun anda tidak memiliki izin<br> untuk mengakses halaman ini!
      </div>
      <div class="modal-footer text-center">
        <?php
          $user = $this->session->userdata('ketpb');
        ?>
        <?php
        if($user != null){
        ?>
          <button type="button" class="btn btn-light" onclick="return logout()" href="javascript:void(0)">Logout</button>
        <?php
        }else{
        ?>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
        <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>