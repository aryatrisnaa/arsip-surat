<div class="sidebar" data-color="purple" data-background-color="white" data-image="<?php echo base_url(); ?>assets/img/sidebar-5.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="<?php echo base_url('')?>" class="simple-text logo-normal">
          ASET SUB BID 2
        </a></div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item <?php if($this->uri->segment(1)=="Dashboard" || $this->uri->segment(1)==""){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url('Dashboard')?>">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>

          <li class="nav-item <?php if($this->uri->segment(1)=="SuratMasuk"){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url('SuratMasuk')?>">
              <i class="material-icons">email</i>
              <p>Surat Masuk</p>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1)=="SuratKeluar"){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url('SuratKeluar')?>">
              <i class="material-icons">forward_to_inbox</i>
              <p>Surat Keluar</p>
            </a>
          </li>
          <li class="nav-item <?php if($this->uri->segment(1)=="PencarianSurat"){echo "active";}?>">
            <a class="nav-link" href="<?php echo base_url('PencarianSurat')?>">
              <i class="material-icons">search</i>
              <p>Pencarian Surat</p>
            </a>
          </li>
        </ul>
      </div>
    </div>