<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("partials/head.php") ?>
        <style type="text/css">
          #table-scroll {
            height:280px;
            overflow:auto;  
            margin-top:5px;
            width:100%;
            background:#fff;
            color:black;
          }
        </style>
    </head>
    <body class="">
  <div class="wrapper ">
    <?php $this->load->view("partials/sidebar.php") ?>
    <div class="main-panel">
      <?php $this->load->view("partials/navbar.php") ?>

      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">

            <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">email</i>
                  </div>
                  <p class="card-category">Jumlah surat masuk</p>
                  <h3 class="card-title"><?php echo $totalMasuk ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-primary card-header-icon">
                  <div class="card-icon">
                    <i class="material-icons">mark_email_read</i>
                  </div>
                  <p class="card-category">Jumlah surat sudah dispo</p>
                  <h3 class="card-title"><?php echo $totalDispo ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                  <i class="material-icons">forward_to_inbox</i>
                  </div>
                  <p class="card-category">Jumlah surat keluar</p>
                  <h3 class="card-title"><?php echo $totalKeluar ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                  </div>
                </div>
              </div>
            </div>
            <!-- ----------- -->
            <?php 
            $akun = $this->session->userdata('nama');
            if ($akun == "KASIDI"){
            ?>
              <div class="col-lg-4 col-md-12 col-sm-12">
                <div class="card card-stats">
                  <div class="card-header card-header-secondary card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">contact_mail</i>
                    </div>
                    <h4 class="card-title">Dispo belum ditanggapi</h4>
                    <?php if($totalDispoBT > 0){?>
                      <h2 class="card-title" style="color:red"><b><?php echo $totalDispoBT ?></b></h2>
                    <?php }
                    else{?>
                     <h2 class="card-title"><b><?php echo $totalDispoBT ?></b></h2>
                    <?php }?>
                  </div>
                  <div class="row justify-content-center form-group">
                    <div id="table-scroll" class="col-lg-12 col-md-12 col-sm-12">
                      <table style="font-size:12px" class="table-bordered" width="100%">
                        <thead>
                          <tr>
                            <th style="text-align:center">No. surat</th>
                            <th style="text-align:center">Dispo ke</th>
                            <th style="text-align:center">Aksi</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($dataBLMTanggap as $a): ?>
                          <tr>
                            <td><?php echo $a->no_surat ?></td>
                            <td><?php echo $a->kepada ?></td>
                            <td><center><a href="<?php echo site_url('SuratMasuk/detail_data/'.$a->id) ?>" class='btn btn-sm btn-secondary'><i class='fa fa-info' ></i> Detail </a></td>
                          </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="stats">
                      <i class="material-icons">update</i> Just Updated
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group col-lg-8 col-md-12 col-sm-12 table-responsive">
              <h5>Tanggapan terbaru dari dispo anda :</h5>
                <table class="table table-bordered table-striped" style="font-size:13px" id="tabel-tanggapan" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th style="text-align:center">No.</th>
                      <th style="text-align:center">No. Surat</th>
                      <th style="text-align:center">Surat Dari</th>
                      <th style="text-align:center">Perihal</th>
                      <th style="text-align:center">Dispo ke</th>
                      <th style="text-align:center">Tanggapan</th>
                      <th style="text-align:center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
            <?php
            }
            else{
            ?>
              <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="card card-stats">
                  <div class="card-header card-header-secondary card-header-icon">
                    <div class="card-icon">
                      <i class="material-icons">contact_mail</i>
                    </div>
                    <h4 class="card-title">Jumlah surat dispo ke anda</h4>
                    <b><h2 class="card-title"><?php echo $totalDispoAnda ?></h2>
                  </div>
                  <div class="row justify-content-center form-group">
                    <div class="col-lg-11 col-md-11 col-sm-11">
                      <table class="table" width="100%">
                        <tr>
                          <td width="30%">Belum ditanggapi :</td>
                          <?php if($totalDispoAndaBT > 0){?>
                            <td style="text-align:left"><h3 style="color:red"><b><?php echo $totalDispoAndaBT ?></b></h3></td>
                          <?php }
                          else{?>
                            <td style="text-align:left"><h3><b><?php echo $totalDispoAndaBT ?></b></h3></td>
                          <?php }?>
                          <td width="30%">
                            <input type="hidden" id="belum" value="" class="form-control hidden" name="belum">
                            <button type="submit" id="tombol-belum" class='btn btn-sm btn-secondary col-md-12' onclick="myFunction()"><i class='fa fa-info' ></i> Lihat </button>
                          </td>
                        </tr>
                        <tr>
                          <td width="30%">Sudah ditanggapi :</td>
                          <td><h3><b><?php echo $totalDispoAndaT ?></b></h3></td>
                          <td width="30%">
                          <input type="hidden" id="sudah" value="" class="form-control hidden" name="sudah">
                          <button type="submit" id="tombol-sudah" class='btn btn-sm btn-secondary col-md-12' onclick="myFunction2()"><i class='fa fa-info' ></i> Lihat </button>
                          </td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="card-footer">
                    <div class="stats">
                      <i class="material-icons">update</i> Just Updated
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row justify-content-center">
              <div class="form-group col-lg-11 col-md-11 col-sm-12 table-responsive">
                <table class="table table-bordered table-striped" style="font-size:13px" id="tabel-dispo-anda-belum" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th style="text-align:center">No.</th>
                      <th style="text-align:center">No. Surat</th>
                      <th style="text-align:center">Surat Dari</th>
                      <th style="text-align:center">Perihal</th>
                      <th style="text-align:center">Isi Dispo</th>
                      <th style="text-align:center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
            <?php
            }
            ?>
          
        </div>
      </div>
      
      <!-- Footer -->
      <!-- </?php $this->load->view("partials/footer.php") ?> -->

    </div>
  </div>

  <!-- JS -->
  <?php $this->load->view("partials/js.php") ?>
  <script type="text/javascript">
    $(document).ready(function() {
      var table = $('#tabel-dispo-anda-belum').DataTable({
        "processing": true,
        "serverSide": true,
        pageLength : 5,
        "searching": false,
        "lengthChange": false,
        "orderMulti": true,
        "ajax": {
          "dataType": "json",
          "type": "POST",
          "url" : "Dashboard/data_dispo_anda",
          "data": function(d) {
              return $.extend({}, d, {
                  "belum": $('#belum').val(),
                  "sudah": $('#sudah').val(),
              });
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "no_surat", "className": "text-center", 'sortable': false},
          { "data": "dari", 'sortable': false},
          { "data": "judul", 'sortable': false},
          { "data": "isi", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#tabel-dispo-anda-belum_filter input').unbind();
      $('#tabel-dispo-anda-belum_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
      });
      $('#refresh').bind('click', function() {
        $('#tabel-dispo-anda-belum').DataTable().ajax.reload();
      });
      var dtable = $('#tabel-dispo-anda-belum').dataTable().api();
      $('#tombol-belum').click(function() {
          dtable.draw();
      });
      $('#tombol-sudah').click(function() {
          dtable.draw();
      });
    });


    $(document).ready(function() {
      var table = $('#tabel-tanggapan').DataTable({
        "processing": true,
        "serverSide": true,
        pageLength : 3,
        "searching": false,
        "lengthChange": false,
        "orderMulti": true,
        "ajax": {
          "url": "Dashboard/data_tanggapan",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "no_surat", "className": "text-center", 'sortable': false},
          { "data": "dari", 'sortable': false},
          { "data": "judul", 'sortable': false},
          { "data": "kepada", 'sortable': false},
          { "data": "tanggapan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function (row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });

      $('#tabel-tanggapan_filter input').unbind();
      $('#tabel-tanggapan_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#tabel-tanggapan').DataTable().ajax.reload();
      });
    });

    function detail_tanggapan(id){
      window.location = '<?=base_url();?>'+'/SuratMasuk/detail_data/'+id;
    }
    function detail_data(id){
      window.location = '<?=base_url();?>'+'/SuratMasuk/detail_data/'+id;
    }
  </script>
  <script>
    function myFunction() {
      document.getElementById("belum").value = "belum";
      document.getElementById("sudah").value = "";
    }
    function myFunction2() {
      document.getElementById("sudah").value = "sudah";
      document.getElementById("belum").value = "";
    }
  </script>

  </body>
