<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view("partials/head.php") ?>
    </head>
    <body class="">
  <div class="wrapper ">
    <?php $this->load->view("partials/sidebar.php") ?>
    <div class="main-panel">
      <?php $this->load->view("partials/navbar.php") ?>

      <div class="content">
        <div class="container-fluid">
          <div class="row justify-content-center">

            <!-- ----------- -->
            <div class="col-lg-10 col-md-11 col-sm-12">
              <div class="card">
                <div class="card-header form-group">
                  <p class="card-category" style="font-size:20px; text-align:center; font-weight:bold;">PENCARIAN DATA SURAT</p><br>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-8">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <input name="cari" id="cari" class="form-control" type="text" placeholder="masukkan kata kunci pencarian.." required>
                      </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-lg-4">
                      <div class="row justify-content-center form-group">
                        <button class="btn btn-sm btn-success col-md-12 col-sm-12 col-xs-12" type="submit" style="color:#fff" id="pencarian"><i class="fa fa-search"></i>Cari</button>
                      </div>                     
                    </div>
                  
                  </div>

                </div>
              </div>
            </div>
            <div class="form-group col-lg-10 col-md-11 col-sm-12 table-responsive">
            <table class="table table-bordered table-striped" style="font-size:14px" id="tabel-cari" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th style="text-align:center">No.</th>
                  <th style="text-align:center">Tanggal Surat</th>
                  <th style="text-align:center">No. Surat</th>
                  <th style="text-align:center">Perihal</th>
                  <th style="text-align:center">Dari/kepada</th>
                  <th style="text-align:center">Keterangan</th>
                  <th style="text-align:center">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
            </div>
          </div>
          
        </div>
      </div>
      
      <!-- Footer -->
      <!-- </?php $this->load->view("partials/footer.php") ?> -->

    </div>
  </div>

  <!-- JS -->
  <?php $this->load->view("partials/js.php") ?>
  <script type="text/javascript">
    $(document).ready(function() {
      var table = $('#tabel-cari').DataTable({
        "processing": true,
        "serverSide": true,
        "searching": false,
        pageLength : 5,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "dataType": "json",
          "type": "POST",
          "url" : "PencarianSurat/data_pencarian",
          "data": function(d) {
              return $.extend({}, d, {
                  "cari": $('#cari').val(),
              });
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "tgl_surat", "className": "text-center", 'sortable': false},
          { "data": "no_surat", 'sortable': false},
          { "data": "judul", 'sortable': false},
          { "data": "objek", 'sortable': false},
          { "data": "keterangan", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function(row, data, index) {
          var info = table.page.info();
          var value = index + 1 + info.start;
          $('td', row).eq(0).html(value);
        }
      });
      $('#tabel-cari_filter input').unbind();
      $('#tabel-cari_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
      });
      $('#refresh').bind('click', function() {
        $('#tabel-cari').DataTable().ajax.reload();
      });
      var dtable = $('#tabel-cari').dataTable().api();
      $('#pencarian').click(function() {
          dtable.draw();
      });
    });

    function detail_data(id){
      window.location = '<?=base_url();?>'+'/PencarianSurat/detail_data/'+id;
    }
  </script>
  
  <!-- fungsi untuk klik tombol submit dengan enter -->
  <script>
    var input = document.getElementById("cari");
    input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("pencarian").click();
      }
    });
  </script>
  </body>
