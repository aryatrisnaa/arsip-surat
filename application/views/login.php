<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Sign in</title>
    <?php $this->load->view("partials/head.php") ?>
    <style type="text/css">
        body {
        background-color: #acbdad;
        font-family: 'Ubuntu', sans-serif;
        }
    
        .main {
            background-color: #FFFFFF;
            width: 400px;
            height: 320px;
            margin: 15% auto;
            border-radius: 1.5em;
            box-shadow: 0px 11px 35px 2px rgba(0, 0, 0, 0.14);
        }
    
        .sign {
            padding-top: 40px;
            color: #4c971a;
            font-family: 'Ubuntu', sans-serif;
            font-weight: bold;
            font-size: 23px;
        }
    
        .un {
            width: 76%;
            color: rgb(38, 50, 56);
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 1px;
            background: rgba(136, 126, 126, 0.04);
            padding: 10px 20px;
            border: none;
            border-radius: 20px;
            outline: none;
            box-sizing: border-box;
            border: 2px solid rgba(0, 0, 0, 0.02);
            margin-bottom: 50px;
            margin-left: 46px;
            text-align: center;
            margin-bottom: 27px;
            font-family: 'Ubuntu', sans-serif;
        }
    
        form.form1 {
            padding-top: 50px;
        }
    
        .pass {
            width: 76%;
            color: rgb(38, 50, 56);
            font-weight: 700;
            font-size: 14px;
            letter-spacing: 1px;
            background: rgba(136, 126, 126, 0.04);
            padding: 10px 20px;
            border: none;
            border-radius: 20px;
            outline: none;
            box-sizing: border-box;
            border: 2px solid rgba(0, 0, 0, 0.02);
            margin-bottom: 50px;
            margin-left: 46px;
            text-align: center;
            margin-bottom: 27px;
            font-family: 'Ubuntu', sans-serif;
        }
    
   
        .un:focus, .pass:focus {
            border: 2px solid rgba(0, 0, 0, 0.18) !important;
            
        }
    
        .submit {
            cursor: pointer;
            border-radius: 5em;
            color: #fff;
            background: linear-gradient(to right, #a3d7a5, #4c971a);
            border: 0;
            padding-left: 40px;
            padding-right: 40px;
            padding-bottom: 10px;
            padding-top: 10px;
            font-family: 'Ubuntu', sans-serif;
            font-size: 13px;
            box-shadow: 0 0 20px 1px rgba(0, 0, 0, 0.04);
        }
    
        .forgot {
            text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12);
            color: #E1BEE7;
            padding-top: 15px;
        }
    
        a {
            text-shadow: 0px 0px 3px rgba(117, 117, 117, 0.12);
            color: #E1BEE7;
            text-decoration: none
        }
    
        @media (max-width: 600px) {
            .main {
                border-radius: 20px;
				width: 320px;
				height: 300px;
				margin: 45% auto;
            }
        }
    </style>
</head>
<body>
    <div class="tengah">
        <form role="form" class="form-login" method="POST" id="form-login">
        <div class="main">
            <p class="sign" align="center">Sign in</p><br>
            <form class="form1">
            <input class="un form-control" type="text" align="center" placeholder="Username" name="user" id="user">
            <input class="pass form-control" type="password" align="center" placeholder="Password" name="pass" id="pass">
            <center><button class="submit" type="submit"><i class="fa fa-lock"></i> SIGN IN</button></center>
        </div>
        </form>
    </div>
    
    <?php $this->load->view("partials/alert.php") ?>
    <?php $this->load->view("partials/js.php") ?>

    <script>
    $('#form-login').submit(function(event) {
      event.preventDefault();
      var user = $('#user').val();
      var pass = $('#pass').val();

      if (!user) {
        $("#alert_warning_login").trigger("click");
        return false;
      }
      if (!pass) {
        $("#alert_warning_login").trigger("click");
        return false;
      }

      $.ajax({
        type: "POST",
        url: "Login/logon",
        data: $('#form-login').serialize(),
        success: function(response) {
          var result = JSON.parse(response);
          
          if (result.status == 422) {
            $("#alert_warning_login").trigger("click");
          }
          else{
            window.location = "<?php echo base_url('Dashboard')?>";
          }
        },
        error: function(response) {
          $("#alert_warning_login").trigger("click");
        }
      });

    });
  </script>
</body>
</html>