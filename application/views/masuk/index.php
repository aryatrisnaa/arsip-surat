<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
</head>

<body class="">
  <div class="wrapper ">
    <?php $this->load->view("partials/sidebar.php") ?>
    <div class="main-panel">
    <?php $this->load->view("partials/navbar.php") ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><b>Daftar Surat Masuk</b></h4>
                  <?php
                    if($this->session->flashdata('success')){
                      ?>
                      <div class="alert alert-success text-center" style="margin-top:20px;">
                        <?php echo $this->session->flashdata('success'); ?>
                      </div>
                      <?php
                    }

                    if($this->session->flashdata('error')){
                      ?>
                      <div class="alert alert-danger text-center" style="margin-top:20px;">
                        <?php echo $this->session->flashdata('error'); ?>
                      </div>
                      <?php
                    }
                    ?>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <div class="pull-right"><a class="btn btn-success btn-sm" style="color:#fff" data-toggle="modal" data-target="#modal_masuk_add_new"><span class="material-icons">add_task</span> Tambah Data</a></div>
                    <br><br>
                    <table class="table table-bordered table-striped" id="masuk-table" width="100%" cellspacing="0">
                      <thead>
                        <tr class="info">
                          <th style="text-align:center">No</th>
                          <th style="text-align:center">Tanggal Surat</th>
                          <th style="text-align:center">No Surat</th>
                          <th style="text-align:center">Perihal</th>
                          <th style="text-align:center">Dari</th>
                          <th style="text-align:center">Tanggal Upload</th>
                          <th style="text-align:center">Keterangan</th>
                          <th style="text-align:center">Status</th>
                          <th style="text-align:center" width="19%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- footer -->
      <!-- </?php $this->load->view("partials/footer.php") ?> -->
    </div>
  </div>

  <!-- ============ MODAL ADD Surat =============== -->
  <div class="modal fade" id="modal_masuk_add_new" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <center><h3 >Tambah Data Surat Masuk</h3></center>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>Suratmasuk/simpan_masuk" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="form-group">
                <label class="col-md-3">Tanggal Surat :</label><br>
                <div class="col-md-12">
                  <input type="text" class="form-control input-tanggal" name="surat_tanggal" id="surat_tanggal" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">No Surat :</label><br>
                <div class="col-md-12">
                  <input name="no_surat" id="no_surat" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Perihal :</label><br>
                <div class="col-md-12">
                  <input name="surat_judul" id="surat_judul" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Dari :</label><br>
                <div class="col-md-12">
                  <input name="surat_dari" id="surat_dari" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Keterangan :</label><br>
                <div class="col-md-12">
                  <textarea class="form-control" id="surat_keterangan" name="surat_keterangan" placeholder="" cols="30" rows="5"></textarea>
                </div>
              </div><br>

              <div class="col-md-12">
                <label>File:</label><br>
                <input type="file" name="upload" id="upload" required>
                <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : pdf</p>
              </div>
            </div>
        </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL ADD Surat-->

  <!-- ============ MODAL edit Surat =============== -->
  <div class="modal fade" id="modal_masuk_edit" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <center><h3 >Edit Data Surat Masuk</h3></center>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>Suratmasuk/update_data" enctype="multipart/form-data">
            <div class="modal-body">

            <input type="hidden" id="edit_surat_id" class="form-control hidden" name="edit_surat_id">

              <div class="form-group">
                <label class="col-md-3">Tanggal Surat :</label><br>
                <div class="col-md-12">
                  <input type="text" class="form-control input-tanggal" name="edit_surat_tanggal" id="edit_surat_tanggal" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">No Surat :</label><br>
                <div class="col-md-12">
                  <input name="edit_no_surat" id="edit_no_surat" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Perihal :</label><br>
                <div class="col-md-12">
                  <input name="edit_surat_judul" id="edit_surat_judul" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Dari :</label><br>
                <div class="col-md-12">
                  <input name="edit_surat_dari" id="edit_surat_dari" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Keterangan :</label><br>
                <div class="col-md-12">
                  <textarea class="form-control" id="edit_surat_keterangan" name="edit_surat_keterangan" placeholder="" cols="30" rows="5"></textarea>
                </div>
              </div><br>

            </div>
        </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL edit Surat-->

  <!-- ============ MODAL ADD dispo =============== -->
  <div class="modal fade" id="modal_dispo" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <center><h3 >Tambah Data Disposisi Sub 2</h3></center>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>SuratMasuk/simpan_dispo" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="form-group">
                <label class="col-md-3">Dispo dari no. surat :</label><br>
                <div class="col-md-12">
                  <input name="get_no_surat" id="get_no_surat" class="form-control" type="text" placeholder="" readonly>
                </div>
              </div><br>

              <div class="">
                <label class="col-md-3">Tanggal Diterima :</label><br>
                <div class="col-md-12">
                  <input type="text" class="form-control input-tanggal" name="tgl_diterima_dispo" id="tgl_diterima_dispo" required>
                </div>
              </div><br>
              
              <div class="form-group">
                <label class="col-md-3">Diteruskan Kepada :</label><br>
                <div class="col-md-12 row">
                  <div class="col-md-6">
                    <p><input type="checkbox" name="kepada_dispo[]" value="NANIK P" /> NANIK P</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="TITIS W" /> TITIS W</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="ARIS B" /> ARIS B</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="DJUNAIDI" /> DJUNAIDI</p>
                  </div>
                  <div class="col-md-6">
                    <p><input type="checkbox" name="kepada_dispo[]" value="SOLEH" /> SOLEH</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="ANGGA KUSUMA D" /> ANGGA KUSUMA D</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="RIZKY WULAN P" /> RIZKY WULAN P</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="HENDRO B" /> HENDRO B</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="A. MUHAJIR" /> A. MUHAJIR</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="NANDA" /> NANDA</p>
                    <p><input type="checkbox" name="kepada_dispo[]" value="ARYA" /> ARYA</p>
                  </div>
                </div>
              </div><br>
              
              <div class="form-group">
                <label class="col-md-3">Isi Dispo :</label><br>
                <div class="col-md-12 row">
                  <div class="col-md-6">
                    <p><input type="checkbox" name="isi_baku[]" value="Tanggapan dan Saran" /> Tanggapan dan Saran</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Proses Lebih Lanjut" /> Proses Lebih Lanjut</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Koordinasi / Konfirmasi" /> Koordinasi / Konfirmasi</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Cermati" /> Cermati</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Cukupi" /> Cukupi</p>
                  </div>
                  <div class="col-md-6">
                    <p><input type="checkbox" name="isi_baku[]" value="Fasilitasi" /> Fasilitasi</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Tindak Lanjut" /> Tindak Lanjut</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Untuk diselesaikan" /> Untuk diselesaikan</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Untuk diketahui" /> Untuk diketahui</p>
                    <p><input type="checkbox" name="isi_baku[]" value="Hadir" /> Hadir</p>
                  </div>
                </div>
                <div class="col-md-12">
                  <textarea class="form-control" name="isi_dispo" id="isi_dispo" placeholder="input isi dispo disini..." cols="30" rows="5"></textarea>
                </div>
              </div><br>
              
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL ADD dispo-->

  <!-- ============ MODAL ADD SURAT BALASAN =============== -->
  <div class="modal fade" id="modal_balas" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <center><h3 >Tambah Data Surat Balasan</h3></center>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>SuratKeluar/simpan_keluar" enctype="multipart/form-data">
            <div class="modal-body">
              
              <div class="form-group">
                <label class="col-md-3">Membalas surat nomor :</label><br>
                <div class="col-md-12">
                  <input name="get_no_surat_balas" id="get_no_surat_balas" class="form-control" type="text" placeholder="" readonly>
                </div>
              </div><br>

              <div class="">
                <label class="col-md-3">Tanggal Surat :</label><br>
                <div class="col-md-12">
                  <input type="text" class="form-control input-tanggal" name="surat_tanggal_keluar" id="surat_tanggal_keluar" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">No Surat :</label><br>
                <div class="col-md-12">
                  <input name="no_surat_keluar" id="no_surat_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Perihal :</label><br>
                <div class="col-md-12">
                  <input name="surat_judul_keluar" id="surat_judul_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Kepada :</label><br>
                <div class="col-md-12">
                  <input name="surat_keluar" id="surat_keluar" class="form-control" type="text" placeholder="" required>
                </div>
              </div><br>

              <div class="form-group">
                <label class="col-md-3">Keterangan :</label><br>
                <div class="col-md-12">
                  <textarea class="form-control" id="surat_keterangan_keluar" name="surat_keterangan_keluar" placeholder="" cols="30" rows="5"></textarea>
                </div>
              </div><br>

              <div class="col-md-12">
                <label>File:</label><br>
                <input type="file" name="upload_keluar" id="upload_keluar" required>
                <p style="font-size:14px; color:#A6ACAF ">*file yang didukung : pdf</p>
              </div>
            </div>
        </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL ADD SURAT BALASAN-->
  
  <!-- JS -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>

  <script type="text/javascript">
    $(document).ready(function() {

      $('.input-tanggal').daterangepicker({
        format          : "dd-mm-yyyy",
        singleDatePicker: true,
        autoApply       : true,
        todayHighlight  : true,
        locale: {
          format: "DD-MM-YYYY",
          // separator: " - ",
        }
      });
      
      var table = $('#masuk-table').DataTable({
        "processing": true,
        "serverSide": true,
        // "order": [],
        "orderMulti": true,
        "ajax": {
          "url": "SuratMasuk/data_masuk",
          "dataType": "json",
          "type": "POST",
          "data": {
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
          }
        },
        "columns": [
          { "data": null, "className": "text-center", 'sortable': false},
          { "data": "tgl_surat", 'sortable': false},
          { "data": "no_surat", "className": "text-center", 'sortable': false},
          { "data": "judul", 'sortable': false},
          { "data": "dari", 'sortable': false},
          { "data": "created_at", 'sortable': false},
          { "data": "keterangan", 'sortable': false},
          { "data": "status", 'sortable': false},
          { "data": "action", 'sortable': false},
        ],
        fnCreatedRow: function (row, data, index) {
					var info = table.page.info();
					var value = index + 1 + info.start;
					$('td', row).eq(0).html(value);
				}
      });
      
      $('#masuk-table_filter input').unbind();
      $('#masuk-table_filter input').bind('keyup', function(e) {
        if (e.keyCode == 13 || $(this).val().length == 0) {
          table.search($(this).val()).draw();
        }
        // if ($(this).val().length == 0 || $(this).val().length >= 3) {
        //     table.search($(this).val()).draw();
        // }
      });
      $('#refresh').bind('click', function() {
        $('#masuk-table').DataTable().ajax.reload();
      });
    });

      // EDIT data
      function edit_data(id){
        $.ajax({
          type: "get",
          url: 'SuratMasuk/edit_data/'+id,
        })
        .done(function (response) {
            var result = JSON.parse(response)
            
            $('#edit_surat_id').val(result.query.id);
            $('#edit_surat_tanggal').val(result.query.tgl_surat);
            $('#edit_no_surat').val(result.query.no_surat);
            $('#edit_surat_judul').val(result.query.judul);
            $('#edit_surat_dari').val(result.query.dari);
            $('#edit_surat_keterangan').val(result.query.keterangan);
            
            $("#modal_masuk_edit").modal('show');
            
        });
      }

      function delete_data(id){
        var url = '<?php echo site_url('SuratMasuk/delete_data/') ?>'+id;
        // console.log(url);
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
      }

      function add_dispo(id) {
			$.ajax({
					type: "get",
					url: 'Suratmasuk/getNoSurat/' + id,
				})
				.done(function (response) {
					var result = JSON.parse(response)
					$('#get_no_surat').val(result.query.no_surat);

					$("#modal_dispo").modal('show');

				});
      }
      
      function add_balasan(id) {
			$.ajax({
					type: "get",
					url: 'Suratmasuk/getNoSurat/' + id,
				})
				.done(function (response) {
					var result = JSON.parse(response)
					$('#get_no_surat_balas').val(result.query.no_surat);

					$("#modal_balas").modal('show');

				});
		  }

      function downloadfile(id){
        window.location = '<?=base_url();?>'+'SuratMasuk/download/'+id;
      }

      function detail_data(id){
        window.location = '<?=base_url();?>'+'SuratMasuk/detail_data/'+id;
      }
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#surat_keterangan' ) )
        .catch( error => {
            console.error( error );
        } );
	ClassicEditor
        .create( document.querySelector( '#edit_surat_keterangan' ) )
        .catch( error => {
            console.error( error );
        } );
	ClassicEditor
        .create( document.querySelector( '#isi_dispo' ) )
        .catch( error => {
            console.error( error );
        } );
	ClassicEditor
        .create( document.querySelector( '#surat_keterangan_keluar' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
</body>

</html>