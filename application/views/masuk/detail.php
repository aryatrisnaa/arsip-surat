<!DOCTYPE html>
<html lang="en">

<head>
  <?php $this->load->view("partials/head.php") ?>
  <script src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
</head>

<body class="">
  <div class="wrapper ">
    <?php $this->load->view("partials/sidebar.php") ?>
    <div class="main-panel">
    <?php $this->load->view("partials/navbar.php") ?>
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title "><b>Detail Surat Masuk</b></h4>
                </div>
                <div class="card-body"><br>
                  <div class="col-lg-12 col-md-12 col-sm-12 row">
                  <?php foreach ($detail as $s): ?>
                    <div class="col-lg-4 col-md-12 col-sm-12">
                        <table class="table table-bordered">
                            <tr>
                                <td><b>Status</b></td>
                                <td>
                                  <?php 
                                  if($s->dispo == NULL){ ?>
                                    <center><a class='btn btn-sm btn-danger' style='color:#fff'>BELUM DIPROSES</a></center>
                                  <?php 
                                  }else{
                                    if($s->status_dispo == NULL){?>
                                      <center><a class='btn btn-sm btn-warning' style='color:#fff'>TELAH DIPROSES</a></center>;
                                    <?php 
                                    }else if ($s->balasan == NULL){?>
                                      <center><a class='btn btn-sm btn-success' style='color:#fff'><span class='material-icons'>perm_identity</span>SELESAI</a></center>;
                                    <?php 
                                    }else{?>
                                      <center><a class='btn btn-sm btn-primary' style='color:#fff'><span class='material-icons'>forward_to_inbox</span>TELAH DIBALAS</a></center>;
                                    <?php 
                                    }
                                  } ?>
                                </td>            
                            </tr> 
                            <tr>
                                <td><b>Tanggal Surat</b></td>
                                <td><a><?php echo $s->tgl_surat ?></a></td>            
                            </tr> 
                            <tr>
                                <td><b>No. Surat</b></td>
                                <th><a><?php echo $s->no_surat ?></a></th>            
                            </tr>
                            <tr>
                                <td><b>Perihal</b></td>
                                <td><a><?php echo $s->judul ?></a></td>            
                            </tr>
                            <tr>
                                <td><b>Dari</b></td>
                                <td><a><?php echo $s->dari ?></a></td>            
                            </tr>           
                            <tr>
                                <td><b>Keterangan</b></td>
                                <td><a><?php echo $s->keterangan ?></a></td>            
                            </tr>
                            <tr>
                                <td><b>Tanggal Upload</b></td>
                                <td><a><?php echo substr($s->created_at,0,19) ?></a></td>            
                            </tr>

                        </table>       
                    </div>

                    <div class="col-lg-8 col-md-12 col-sm-12 row justify-content-center">
                      <table class="table table-responsive">
                        <tr>
                          <embed type="application/pdf" src="<?php echo base_url('upload/masuk/'.$s->nama_file); ?>" width="650" height="400"></embed>              
                        </tr>
                      </table>
                    </div>
                    <?php endforeach; ?>
                  </div>

                  <br>
                  <h4 align="center">DISPOSISI</h4>
                  <div class="table-responsive container col-sm-8">
                    <?php
                      $user = $this->session->userdata('nama');
                      if(count($disposisi)>0)
                    {?>
                    <table class="table table-bordered" width="100%" cellspacing="0">
                      <?php foreach ($disposisi as $s): ?>
                        <tr>
                          <td style="text-align:left" width="150px">No Surat</td>
                          <th style="text-align:left">
                            <a><?php echo $s->no_surat ?></a>
                          </th>
                        </tr>
                        <tr>
                          <td style="text-align:left" width="150px">Kepada</td>
                          <td style="text-align:left">
                            <a><?php echo $s->kepada ?></a>
                          </td>
                        </tr>
                        <tr>
                          <td style="text-align:left" width="150px">Tanggal Diterima</td>
                          <td style="text-align:left">
                            <a><?php echo $s->tgl_diterima ?></a>
                          </td>
                        </tr>
                        <tr>
                          <td style="text-align:left" width="150px">Isi</td>
                          <td style="text-align:justify">
                            <a><?php echo $s->isi_baku ?></a><br>
                            <a><?php echo $s->isi ?></a>
                          </td>
                        </tr>
                        <?php 
                        if(stripos($s->kepada, $user) !== FALSE) {
                          if($s->status_dispo == NULL) { ?>
                            <tr>
                              <td colspan="2" style="text-align:center"><a class="btn btn-warning btn-sm" style="color:#fff" data-toggle="modal" data-target="#modal_tanggapan"><span class="material-icons">add</span> Tanggapi</a></td>
                            </tr>
                          <?php 
                          }
                          else { ?>
                            <tr>
                              <td colspan="2" style="text-align:center"><button type="button" class="btn disabled btn-success btn-sm" style="color:#fff" ><span class="material-icons">check</span>Telah Ditanggapi</button></td>
                            </tr>
                         <?php 
                          }?>  
                        <?php 
                        }
                        
                        if ($s->status_dispo != NULL){?>
                          <tr>
                            <td colspan="2" style="text-align:center"><button type="button" class="btn disabled btn-success btn-sm" style="color:#fff" ><span class="material-icons">check</span>Telah Ditanggapi</button></td>
                          </tr>
                          <tr>
                            <td style="text-align:left" width="150px">Tanggapan</td>
                            <td style="text-align:justify"><?php echo $s->tanggapan ?></td>
                          </tr>
                        <?php 
                        }
                    
                    endforeach; ?>  
                    </table>
                    <?php
                    }
                    else
                    {
                      ?><h6 align="center"><?php echo "- Belum ada Disposisi -";?></h6><?php
                    }
                    ?>
                  </div>  
        
                  <br>
                  <h4 align="center">Balasan Surat</h4>
                  <div class="table-responsive container col-sm-12">
                    <?php
                      if(count($balasan)>0)
                    {?>
                    <table class="table table-bordered" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th style="text-align:center">Tanggal Surat Balasan</th>
                          <th style="text-align:center">No Surat Balasan</th>
                          <th style="text-align:center">Perihal</th>
                          <th style="text-align:center">Kepada</th>
                          <th style="text-align:center">File</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($balasan as $s): ?>
                          <tr>
                            <td style="text-align:center">
                                <a><?php echo $s->tgl_surat ?></a>
                            </td>
                            <td style="text-align:center">
                                <a><?php echo $s->no_surat ?></a>
                            </td>
                            <td style="text-align:center">
                                <a><?php echo $s->judul ?></a>
                            </td>
                            <td style="text-align:center">
                                <a><?php echo $s->keluar ?></a>
                            </td>
                            <td style="text-align:center">
                              <a href="<?php echo site_url('SuratKeluar/download/'.$s->id) ?>" class="btn btn-secondary btn-sm"><i class="fa fa-download"></i> Download</a>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                    <?php
                    }
                    else
                    {
                      ?><h6 align="center"><?php echo "- Tidak ada Balasan -";?></h6><?php
                    }
                    ?>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- footer -->
      <!-- </?php $this->load->view("partials/footer.php") ?> -->
    </div>
  </div>
  
<!-- ============ MODAL ADD dispo =============== -->
<div class="modal fade" id="modal_tanggapan" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <center><h3 >Tambah Data Disposisi Sub 2</h3></center>
        <div class="modal-body">
          <form class="form-horizontal" method="POST" action="<?php echo base_url(); ?>SuratMasuk/simpan_tanggapan" enctype="multipart/form-data">
            <div class="modal-body">
                          
            <input type="hidden" id="id_dispo" class="form-control hidden" value="<?php echo $s->id_dispo ?>" name="id_dispo">

              <div class="form-group">
                <div class="col-md-12 row">
                  <div class="col-md-6">
                    <p><input type="checkbox" name="status" value="done" required/> Menanggapi</p>
                  </div>
                </div>
                <div class="col-md-12">
                  <textarea class="form-control" name="tanggapan" id="tanggapan" placeholder="input tanggapan disini..." cols="30" rows="5"></textarea>
                </div>
              </div><br>
              
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
        </form>
				
      </div>
    </div>
  </div>
  <!--END MODAL ADD dispo-->

  <!-- JS -->
  <?php $this->load->view("partials/js.php") ?>
  <?php $this->load->view("partials/modal.php") ?>

  <script type="text/javascript">
    $('#dataTable').DataTable({
        "ordering": true
    });

    document.getElementById(BUTTON_ID).disabled = true;

    // EDIT data
    // function tanggapan(id){
    //     $.ajax({
    //       type: "get",
    //       url: 'SuratMasuk/tanggapan/'+id,
    //     })
    //     .done(function (response) {
    //         var result = JSON.parse(response)
            
    //         $('#id_dispo').val(result.query.id_dispo);
          
    //         $("#modal_tanggapan").modal('show');
            
    //     });
    //   }
</script>
<script>
	ClassicEditor
        .create( document.querySelector( '#tanggapan' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
</body>

</html>