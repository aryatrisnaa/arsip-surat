<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SuratKeluar extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('SuratKeluar_model');
		// $this->load->model('Login_model');
	}

	public function index()
	{
		$check_session = $this->session->userdata('nama');
        if ($check_session != null) {
			$this->load->view('keluar/index');
		} 
		else{
			redirect('Login');
		}
	}

	public function data_keluar()
	{
		$columns = array(
			0 => 'SuratKeluar.created_at',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = ' desc ';
		$draw  = $this->input->post('draw');

		$totalData = $this->SuratKeluar_model->all_SuratKeluar_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->SuratKeluar_model->all_SuratKeluar_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->SuratKeluar_model->search_SuratKeluar_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->SuratKeluar_model->search_SuratKeluar_count($search);
		}

		$data       = array();
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp   = '"'.$value->id.'"';
				$nestedData['no_surat']   = ucwords($value->no_surat);
				$nestedData['judul'] = ucwords($value->judul);
				$nestedData['keluar']     = ucwords($value->keluar);
				$nestedData['tgl_surat']  = ucwords($value->tgl_surat);
				$nestedData['created_at']     = ucwords($value->created_at);
				$nestedData['keterangan']     = ucwords($value->keterangan);
				$nestedData['action']      = "<center>
				<a onclick='return downloadfile($_temp)' class='btn btn-secondary btn-sm col-md-12'><i class='fa fa-download'></i> Download File</a>
				<br><a onclick='return detail_data($_temp)' class='btn btn-sm btn-secondary col-md-6'><i class='fa fa-info' ></i> Detail </a>
				<a onclick='return edit_data($_temp)' class='btn btn-sm btn-warning col-md-2'><i class='fa fa-pencil' style='margin-left:-5px'></i> </a>
				<a onclick='return delete_data($_temp)' class='btn btn-sm btn-danger col-md-2'><i class='fa fa-trash' style='margin-left:-5px'></i> </a></center>";

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_keluar()
	{
		$check_session = $this->session->userdata('nama');
		
		if ($check_session != null) {

	 	//Check if file is not empty
			if(!empty($_FILES['upload_keluar']['name'])){
				$config['upload_path'] = 'upload/keluar';
				//restrict uploads to this mime types
				$config['allowed_types'] = 'pdf';
				$config['file_name'] = $_FILES['upload_keluar']['name'];
				
				//Load upload library and initialize configuration
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
			
				if($this->upload->do_upload('upload_keluar')){

					$now = date('d-m-Y H:i:s');
					$uploadData = $this->upload->data();
					$nama = $uploadData['file_name'];
					$penginput = $this->session->userdata('nama');

					$data = array(
						'id'         	=> $this->uuid->v4(),
						'balasan_surat' => $this->input->post('get_no_surat_balas'),
						'no_surat'   	=> $this->input->post('no_surat_keluar'),
						'tgl_surat'     => date('d-m-Y',strtotime($this->input->post('surat_tanggal_keluar'))),
						'judul'			=> $this->input->post('surat_judul_keluar'),
						'keluar'      	=> $this->input->post('surat_keluar'),
						'keterangan'    => $this->input->post('surat_keterangan_keluar'),
						'nama_file' 	=> $nama,
						'created_by' 	=> $penginput,
						'created_at' 	=> $now,
					);

					$cek = $this->SuratKeluar_model->save($data);
					header('location:'.base_url("SuratKeluar"));
					$this->session->set_flashdata('success','File berhasil diunggah, Matursuwun.');

				}else{
					header('location:'.base_url("SuratKeluar"));
					$this->session->set_flashdata('error','File tidak berhasil diunggah!'); 
				}
			}else{
				header('location:'.base_url("SuratKeluar"));
				$this->session->set_flashdata('error','File tidak boleh kosong!');
			}
		} else {

			redirect('login');
		}
	}

	public function detail_data($id){
		//tampil detail surat masuk
		$detail = $this->db->query("SELECT SuratKeluar.*, SuratMasuk.id as id_masuk from SuratKeluar 
		left join SuratMasuk on SuratMasuk.no_surat = SuratKeluar.balasan_surat
		where SuratKeluar.id = '$id'")->result();
		$x["detail"] = $detail;

		$this->load->view('keluar/detail',$x);
		
	  }

	public function download($id){
        $this->load->helper('download');
        $fileinfo = $this->SuratKeluar_model->download($id);
        $file = 'upload/keluar/'.$fileinfo['nama_file'];
        force_download($file, NULL);
    }

	public function edit_data($id)
	{
		$query = $this->db->query("SELECT * FROM SuratKeluar where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_data()
	{
		$id   = $this->input->post('edit_surat_id_keluar');
		$data = array(
			'tgl_surat'		=> date('d-m-Y',strtotime($this->input->post('edit_surat_tanggal_keluar'))),
			'no_surat' 		=> $this->input->post('edit_no_surat_keluar'),
			'judul' 		=> $this->input->post('edit_surat_judul_keluar'),
			'keluar' 		=> $this->input->post('edit_surat_keluar'),
			'keterangan'	=> $this->input->post('edit_surat_keterangan_keluar'),
		);
				
		$this->db->where('id', $id);
		$this->db->update('SuratKeluar', $data);
		redirect("SuratKeluar");
	}

	public function delete_data($id=null)
    {   
		$check_session = $this->session->userdata('nama');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$nik = $this->session->userdata('nama');
				$now = date('d-m-Y H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $nik,
				);
						
				$this->db->where('id', $id);
				$this->db->update('SuratKeluar', $data);
						
				header('location:'.base_url("SuratKeluar"));
				$this->session->set_flashdata('success','Data berhasil dihapus!');
			}
		} else {

			redirect('login');
		}
	}

	
}
