<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
	{
		parent::__construct();
		$this->load->model('LoginModel');
    }
    
    public function index()
	{   
        $check_session = $this->session->userdata('nama');
        if ($check_session != null) {
            redirect('Dashboard');
        } 
        else{
            $this->load->view("login");
        }
		
	}
	
    
    public function logon($post=NULL)
	{
		$user     = $this->input->post("user");
		$pass   = $this->input->post('pass');

		$pass     = $pass;
		$checking = $this->LoginModel->check_user($user,$pass);

		if ($checking->num_rows() > 0) {
			foreach ($checking->result() as $apps) {
				
				$session_data = array(
					'user'      => $apps->username,
					'nama'     => $apps->nama,
				);
				//set session userdata
				$this->session->set_userdata($session_data);
				$status = 200;
			}
		} else {
			$status = 422;
		}

		
		$data = array(
			'status'     => $status, 
		);
		echo json_encode($data);
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect('Login');
	}

}