<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('SuratMasuk_model');
		$this->load->model('SuratKeluar_model');
		$this->load->model('Dashboard_model');
		$this->load->model('LoginModel');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php Dashboard
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$check_session = $this->session->userdata('nama');
        if ($check_session != null) {
			$total["totalMasuk"] = $this->SuratMasuk_model->all_SuratMasuk_count();
			$total["totalKeluar"] = $this->SuratKeluar_model->all_SuratKeluar_count();
			$total["totalDispo"] = $this->SuratMasuk_model->surat_terdispo_count();
			$total["totalDispoAnda"] = $this->SuratMasuk_model->surat_terdispo_anda_count();
			$total["totalDispoAndaBT"] = $this->SuratMasuk_model->surat_terdispo_anda_blmtanggap();
			$total["totalDispoAndaT"] = $this->SuratMasuk_model->surat_terdispo_anda_ditanggapi();
			$total["totalDispoBT"] = $this->SuratMasuk_model->surat_terdispo_blmtanggap();
			$total["dataBLMTanggap"] = $this->SuratMasuk_model->surat_terdispo_blmtanggap_data();
			// var_dump($totalData);
            $this->load->view('dashboard', $total);
        } 
        else{
            redirect('Login');
        }
		
	}

	public function data_tanggapan()
	{
		$akun = $this->session->userdata('nama');

		$columns = array(
			0 => 'SuratDispo.tgl_ditanggapi',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = ' desc ';
		$draw  = $this->input->post('draw');

		$totalData = $this->Dashboard_model->all_tanggapan_count();

		$totalFiltered = $totalData;

		$master = $this->Dashboard_model->all_tanggapan_data($limit, $start, $order, $dir);

		$data       = array();
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp   = '"'.$value->id.'"';
				$nestedData['no_surat']   	= ucwords($value->no_surat);
				$nestedData['judul'] 		= ucwords($value->judul);
				$nestedData['dari']     	= ucwords($value->dari);
				$nestedData['kepada']  		= ucwords($value->kepada);
				$nestedData['tanggapan']	= ucwords($value->tanggapan);
				$nestedData['action']      	= "<center><a onclick='return detail_tanggapan($_temp)' class='btn btn-sm btn-secondary'><i class='fa fa-info' ></i> Detail </a>";
				

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function data_dispo_anda()
	{
		$columns = array(
			0 => 'SuratMasuk.created_at',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = ' desc ';
		$draw  = $this->input->post('draw');

		$value1    = $this->input->post('belum');
		$value2    = $this->input->post('sudah');
		
		if($value1!=null){
			$totalData = $this->Dashboard_model->all_tanggapan_belum_count();
		}
		else{
			if($value2!=null){
				$totalData = $this->Dashboard_model->all_tanggapan_sudah_count();
			}
			else{
				$totalData = null;
			}
		}

		$totalFiltered = $totalData;

		// if (empty($this->input->post('search')['value'])) {
			if($value1!=null){
				$master = $this->Dashboard_model->all_tanggapan_belum_data($limit, $start, $order, $dir);
			}
			else{
				if($value2!=null){
					$master = $this->Dashboard_model->all_tanggapan_sudah_data($limit, $start, $order, $dir);
				}
				else{
					$master = null;
				}
			}
		// } else {
		// 	$search = $this->input->post('search')['value'];

		// 	$master =  $this->Dashboard_model->search_pencarian_data($limit, $start, $order, $dir, $search, $awal, $akhir);

		// 	$totalFiltered = $this->Dashboard_model->search_pencarian_count($search, $awal, $akhir);
		// }

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp   = '"'.$value->id.'"';
				$nestedData['no_surat']   	= ucwords($value->no_surat);
				$nestedData['judul'] 		= ucwords($value->judul);
				$nestedData['dari']     	= ucwords($value->dari);
				$nestedData['isi']  		= ucwords($value->isi);
				$nestedData['action']      	= "<center><a onclick='return detail_tanggapan($_temp)' class='btn btn-sm btn-secondary'><i class='fa fa-info' ></i> Detail </a>";

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function detail_data($id){
		//tampil detail surat masuk
		$masuk = $this->db->query("SELECT * from SuratMasuk where id = '$id'")->result();

		if($masuk==null){
			redirect('SuratKeluar/detail_data/'.$id);
		}
		else{
			redirect('SuratMasuk/detail_data/'.$id);
		}
		
	  }
}
