<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PencarianSurat extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model');
		$this->load->model('LoginModel');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php Dashboard
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$check_session = $this->session->userdata('nama');
        if ($check_session != null) {
			
			// var_dump($totalData);
            $this->load->view('pencarian');
        } 
        else{
            redirect('Login');
        }
		
	}

	public function data_pencarian()
	{
		$columns = array(
			0 => 'created_at',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = ' DESC ';
		$draw  = $this->input->post('draw');

		$cari    = $this->input->post('cari');
		
		if($cari!=null){
			$totalData = $this->Dashboard_model->all_pencarian_count($cari);
		}
		else{
			$totalData = null;
		}

		$totalFiltered = $totalData;

		// if (empty($this->input->post('search')['value'])) {
			if($cari!=null){
				$master = $this->Dashboard_model->all_pencarian_data($limit, $start, $order, $dir, $cari);
			}
			else{
				$master = null;
			}
		// } else {
		// 	$search = $this->input->post('search')['value'];

		// 	$master =  $this->Dashboard_model->search_pencarian_data($limit, $start, $order, $dir, $search, $awal, $akhir);

		// 	$totalFiltered = $this->Dashboard_model->search_pencarian_count($search, $awal, $akhir);
		// }

		$data       = array();
		$nomor_urut = 0;
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp   = '"'.$value->id.'"';
				$nestedData['no_surat']   = ucwords($value->no_surat);
				$nestedData['judul'] = ucwords($value->judul);
				$nestedData['objek']     = ucwords($value->objek);
				$nestedData['tgl_surat']  = ucwords($value->tgl_surat);
				$nestedData['keterangan']     = ucwords($value->keterangan);

				$nestedData['action']      = "<center>
				<a onclick='return detail_data($_temp)' class='btn btn-sm btn-secondary col-md-12'><i class='fa fa-info' ></i> Detail </a>
				</center>";

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function detail_data($id){
		//tampil detail surat masuk
		$masuk = $this->db->query("SELECT * from SuratMasuk where id = '$id'")->result();

		if($masuk==null){
			redirect('SuratKeluar/detail_data/'.$id);
		}
		else{
			redirect('SuratMasuk/detail_data/'.$id);
		}
		
	  }
}
