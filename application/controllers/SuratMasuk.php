<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SuratMasuk extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$this->load->model('SuratMasuk_model');
		// $this->load->model('LoginModel');
		// $this->load->model('Login_model');
	}

	public function index()
	{
		$check_session = $this->session->userdata('nama');
        if ($check_session != null) {
            $this->load->view('masuk/index');
        } 
        else{
            redirect('Login');
        }
	}

	public function data_masuk()
	{
		$akun = $this->session->userdata('nama');

		$columns = array(
			0 => 'SuratMasuk.created_at',
			1 => 'SuratMasuk.tgl_surat',
		);

		$limit = $this->input->post('length');
		$start = $this->input->post('start');
		$order = $columns[$this->input->post('order')[0]['column']];
		$dir   = ' desc ';
		$draw  = $this->input->post('draw');

		$totalData = $this->SuratMasuk_model->all_SuratMasuk_count();

		$totalFiltered = $totalData;

		if (empty($this->input->post('search')['value'])) {
			$master = $this->SuratMasuk_model->all_SuratMasuk_data($limit, $start, $order, $dir);
		} else {
			$search = $this->input->post('search')['value'];

			$master =  $this->SuratMasuk_model->search_SuratMasuk_data($limit, $start, $order, $dir, $search);

			$totalFiltered = $this->SuratMasuk_model->search_SuratMasuk_count($search);
		}

		$data       = array();
		if (!empty($master)) {
			foreach ($master as $key => $value) {
				$_temp   = '"'.$value->id.'"';
				$nestedData['no_surat']   = ucwords($value->no_surat);
				$nestedData['judul'] = ucwords($value->judul);
				$nestedData['dari']     = ucwords($value->dari);
				$nestedData['tgl_surat']  = ucwords($value->tgl_surat);
				$nestedData['created_at']     = substr($value->created_at,0,19);
				$nestedData['keterangan']     = ucwords($value->keterangan);
				if($value->dispo == NULL){
					$nestedData['status']     = "<center><a class='btn btn-sm btn-danger' style='color:#fff'>BELUM DIPROSES</a></center>";
				}else{
					if($value->status == NULL){
						$nestedData['status']     = "<center><a class='btn btn-sm btn-warning' style='color:#fff'>TELAH DIPROSES</a></center>";
					}else if ($value->balasan == NULL){
						$nestedData['status']     = "<center><a class='btn btn-sm btn-success' style='color:#fff'><span class='material-icons'>perm_identity</span>SELESAI</a></center>";
					}else{
						$nestedData['status']     = "<center><a class='btn btn-sm btn-primary' style='color:#fff'><span class='material-icons'>forward_to_inbox</span>TELAH DIBALAS</a></center>";
					}
				}
				if (($akun == 'kasidi')||($akun == 'KASIDI')){
					$nestedData['action']      = "<center>
					<a onclick='return add_dispo($_temp)' class='btn btn-sm btn-secondary col-md-12' data-toggle='modal' data-target='#modal_dispo'><i class='fa fa-plus' ></i> Tambah Dispo</a>
					<a onclick='return add_balasan($_temp)' class='btn btn-sm btn-secondary col-md-12' data-toggle='modal' data-target='#modal_balas'><i class='fa fa-reply' ></i> Balas Surat</a>
					<a onclick='return downloadfile($_temp)' class='btn btn-secondary btn-sm col-md-12'><i class='fa fa-download'></i> Download File</a>
					<br><a onclick='return detail_data($_temp)' class='btn btn-sm btn-secondary col-md-6'><i class='fa fa-info' ></i> Detail </a>
					<a onclick='return edit_data($_temp)' class='btn btn-sm btn-warning col-md-2'><i class='fa fa-pencil' style='margin-left:-5px'></i> </a>
					<a onclick='return delete_data($_temp)' class='btn btn-sm btn-danger col-md-2'><i class='fa fa-trash' style='margin-left:-5px'></i> </a></center>";
				}
				else{
					$nestedData['action']      = "<center>
					<a onclick='return add_balasan($_temp)' class='btn btn-sm btn-secondary col-md-12' data-toggle='modal' data-target='#modal_balas'><i class='fa fa-reply' ></i> Balas Surat</a>
					<a onclick='return downloadfile($_temp)' class='btn btn-secondary btn-sm col-md-12'><i class='fa fa-download'></i> Download File</a>
					<br><a onclick='return detail_data($_temp)' class='btn btn-sm btn-secondary col-md-12'><i class='fa fa-info' ></i> Detail </a>
					<a onclick='return edit_data($_temp)' class='btn btn-sm btn-warning col-md-3'><i class='fa fa-pencil' style='margin-left:-3px'></i> </a>
					<a onclick='return delete_data($_temp)' class='btn btn-sm btn-danger col-md-3'><i class='fa fa-trash' style='margin-left:-3px'></i> </a></center>";
				}
				

				$data[] = $nestedData;
			}
		}

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
			"recordsTotal"    => intval($totalData),
			"recordsFiltered" => intval($totalFiltered),
			"data"            => $data
		);

		echo json_encode($json_data);
	}

	public function simpan_masuk()
	{
		$check_session = $this->session->userdata('nama');
		
		if ($check_session != null) {
			
			

	 	//Check if file is not empty
			if(!empty($_FILES['upload']['name'])){
				$config['upload_path'] = 'upload/masuk/';
				//restrict uploads to this mime types
				$config['allowed_types'] = 'pdf';
				$config['file_name'] = $_FILES['upload']['name'];
				
					//Load upload library and initialize configuration
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
			
				if($this->upload->do_upload('upload')){

					$now = date('d-m-Y H:i:s');
					$uploadData = $this->upload->data();
					$nama = $uploadData['file_name'];
					$penginput = $this->session->userdata('nama');

					$data = array(
						'id'         	=> $this->uuid->v4(),
						'no_surat'   	=> $this->input->post('no_surat'),
						'tgl_surat'     => date('d-m-Y',strtotime($this->input->post('surat_tanggal'))),
						'judul'			=> $this->input->post('surat_judul'),
						'dari'      	=> $this->input->post('surat_dari'),
						'keterangan'    => $this->input->post('surat_keterangan'),
						'nama_file' 	=> $nama,
						'created_by' 	=> $penginput,
						'created_at' 	=> $now,
					);

					$this->SuratMasuk_model->save($data);
					header('location:'.base_url("SuratMasuk"));
					$this->session->set_flashdata('success','File berhasil diunggah, Matursuwun.');

				}else{
					$error = array('error' => $this->upload->display_errors());
					print_r($error);
				}
			}else{
				header('location:'.base_url("SuratMasuk"));
				$this->session->set_flashdata('error','File tidak boleh kosong!');
			}
		} else {

			redirect('Login');
		}
	}

	public function getNoSurat($id)
	{
		$query = $this->db->query("SELECT no_surat FROM SuratMasuk where id = '$id'")->row();

		$data = array(
			'query'     => $query,
		);
		echo json_encode($data);
	}

	public function detail_data($id){
		//tampil detail surat masuk
		$detail = $this->db->query("SELECT a.*, b.no_surat dispo, b.status_dispo, c.balasan_surat balasan from SuratMasuk a
									left join SuratDispo b on b.no_surat = a.no_surat
									left join SuratKeluar c on c.balasan_surat = a.no_surat 
									where a.id = '$id'
									group by a.id, a.no_surat, a.tgl_surat, a.judul, a.dari, a.nama_file, a.keterangan, a.created_at, a.created_by, a.deleted_at, a.deleted_by,
									b.no_surat, b.status_dispo, c.balasan_surat")->result();
		$x["detail"] = $detail;

		$dispo = $this->db->query("SELECT SuratDispo.*, SuratMasuk.id as id from SuratDispo 
									left join SuratMasuk on SuratMasuk.no_surat = SuratDispo.no_surat
									where id = '$id'")->result();
		$x["disposisi"] = $dispo;

		$balas = $this->db->query("SELECT SuratKeluar.*, SuratMasuk.id as id_masuk from SuratKeluar 
									left join SuratMasuk on SuratMasuk.no_surat = SuratKeluar.balasan_surat
									where SuratMasuk.id = '$id'")->result();
		$x["balasan"] = $balas;

		$this->load->view('masuk/detail',$x);
		
	  }

	public function simpan_dispo()
	{
		$check_session = $this->session->userdata('nama');
		
		if ($check_session != null) {
			
			$this->load->library('session');
		
			$now = date('d-m-Y H:i:s');
			$penginput = $this->session->userdata('nama');
			$for_kepada = '';

			if(!empty($this->input->post("kepada_dispo")) && !empty($this->input->post("isi_baku"))){
				foreach($this->input->post("kepada_dispo") as $kepada_dispo){
					$for_kepada .= '- ' . $kepada_dispo . '<br>';
				}
				$for_kepada = substr($for_kepada, 0, -4);

				foreach($this->input->post("isi_baku") as $isi_baku){
					$for_isi .= $isi_baku . ', ';
				}
				$for_isi = substr($for_isi, 0, -2);
						
				$data = array(
					'id_dispo'      => $this->uuid->v4(),
					'no_surat'   	=> $this->input->post('get_no_surat'),
					'kepada'   		=> $for_kepada,
					'isi_baku'   	=> $for_isi,
					'isi'   		=> $this->input->post('isi_dispo'),
					'tgl_diterima'  => date('d-m-Y',strtotime($this->input->post('tgl_diterima_dispo'))),
					'created_at' 	=> $now,
					'created_by' 	=> $penginput,
				);

				$this->SuratMasuk_model->saveDispo($data);
				header('location:'.base_url("SuratMasuk"));
				$this->session->set_flashdata('success','Dispo berhasil diinput, Matursuwun.');
			}else{
				header('location:'.base_url("SuratMasuk"));
				$this->session->set_flashdata('error','Kepada dan isi dispo tidak boleh kosong!');
			}
		} else {

			redirect('login');
		}
	}

	// public function tanggapan($id)
	// {
	// 	$query = $this->db->query("SELECT * FROM SuratDispo where id_dispo = '$id'")->row();

	// 	// return response()->json($query,200);
	// 	$data = array(
	// 		'query' => $query,
	// 	);
	// 	echo json_encode($data);
	// 	// return $query;
	// }

	public function simpan_tanggapan()
	{
		$check_session = $this->session->userdata('nama');
		
		if ($check_session != null) {
			
			$this->load->library('session');
		
			$now = date('d-m-Y H:i:s');
			$penginput = $this->session->userdata('nama');

			if(!empty($this->input->post("status"))){
				
				$id   = $this->input->post('id_dispo');
				$data = array(
					'status_dispo'   	=> $this->input->post('status'),
					'tanggapan'   		=> $this->input->post('tanggapan'),
					'tgl_ditanggapi' 	=> $now,
					'ditanggapi_oleh' 	=> $penginput,
				);

				$this->db->where('id_dispo', $id);
				$this->db->update('SuratDispo', $data);
				header('location:'.base_url("SuratMasuk"));
				$this->session->set_flashdata('success','berhasil ditanggapi, Matursuwun.');
			}else{
				header('location:'.base_url("SuratMasuk"));
				$this->session->set_flashdata('error','mohon centang kolom checkbox!');
			}
		} else {

			redirect('login');
		}
	}

	public function download($id){
        $this->load->helper('download');
        $fileinfo = $this->SuratMasuk_model->download($id);
        $file = 'upload/masuk/'.$fileinfo['nama_file'];
        force_download($file, NULL);
	}
	public function download_dispo($id){
        $this->load->helper('download');
        $fileinfo = $this->SuratMasuk_model->download_dispo($id);
        $file = 'upload/masuk/dispo/'.$fileinfo['nama_file'];
        force_download($file, NULL);
	}
	
	public function edit_data($id)
	{
		$query = $this->db->query("SELECT * FROM SuratMasuk where id = '$id'")->row();

		// return response()->json($query,200);
		$data = array(
			'query' => $query,
		);
		echo json_encode($data);
		// return $query;
	}

	public function update_data()
	{
		$id   = $this->input->post('edit_surat_id');
		$data = array(
			'tgl_surat'  	=> date('d-m-Y',strtotime($this->input->post('edit_surat_tanggal'))),
			'no_surat' 		=> $this->input->post('edit_no_surat'),
			'judul'			=> $this->input->post('edit_surat_judul'),
			'dari' 			=> $this->input->post('edit_surat_dari'),
			'keterangan'	=> $this->input->post('edit_surat_keterangan'),
		);
				
		$this->db->where('id', $id);
		$this->db->update('SuratMasuk', $data);
		header('location:'.base_url("SuratMasuk"));
		$this->session->set_flashdata('success','Data berhasil disimpan');
	}

	public function delete_data($id=null)
    {   
		$check_session = $this->session->userdata('nama');
		
		if ($check_session != null) {		
			
			if (!isset($id)) show_404();
			else{				
				$user = $this->session->userdata('nama');
				$now = date('d-m-Y H:i:s');
	
				$data = array(
					'deleted_at' => $now,
					'deleted_by' => $user,
				);
						
				$this->db->where('id', $id);
				$this->db->update('SuratMasuk', $data);
						
				header('location:'.base_url("SuratMasuk"));
				$this->session->set_flashdata('success','Data berhasil dihapus!');
			}
		} else {

			redirect('login');
		}
	}

	
}
