-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2020 at 10:40 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surat`
--

-- --------------------------------------------------------

--
-- Table structure for table `keluar`
--

CREATE TABLE `keluar` (
  `id` varchar(11) NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `keluar` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `masuk`
--

CREATE TABLE `masuk` (
  `id` varchar(100) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `tgl_surat` date NOT NULL,
  `judul` varchar(150) NOT NULL,
  `dari` varchar(200) NOT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `nama_file` varchar(50) DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `masuk`
--

INSERT INTO `masuk` (`id`, `no_surat`, `tgl_surat`, `judul`, `dari`, `keterangan`, `nama_file`, `created_at`, `created_by`, `deleted_at`, `deleted_by`) VALUES
('001', 'MSK-01', '2020-11-02', 'Percobaan gagal', 'arya', 'bug test 1', '', '2020-11-04 04:27:17.000000', '', NULL, NULL),
('002', 'MSK-02', '2020-11-01', 'hasshhh', 'lt 12', 'mbuh', '', '2020-11-04 07:36:50.000000', 'wong', NULL, NULL),
('08fb3671-3bb4-4ad4-97b6-f3aa156631e5', 'ASA1XD-2020', '2020-11-09', 'tstt', 'asd', '', 'Bukti_Potongan_Pajak_compressed.pdf', '2020-11-09 02:02:56.000000', NULL, NULL, NULL),
('0dee2502-46f4-4012-a4db-e5ba1b3e429e', 'ASA6XD-2020', '2020-11-08', 'percobaan keenam', 'bpkad', 'test satu dua tiga dicoba', 'Laporan_Bahan_Bakar_Kendaraan_07-09-2020_sd_07-09-', '2020-11-09 02:40:19.000000', NULL, NULL, NULL),
('2860962d-5e7d-46ce-aeda-4b56fa8ad040', 'aaab', '2020-11-09', 'aaa', 'aaa', 'aaa', 'Bukti_Potongan_Pajak_compressed_compressed_(1).pdf', '2020-11-09 03:04:12.000000', NULL, '2020-11-09 03:39:34', NULL),
('50e248a4-71f4-4b3b-bdf4-320b826c1509', 'ASA2XD-2020', '2020-11-05', 'percobaan kembali', 'aset', '', 'Laporan_Bahan_Bakar_Kendaraan_06-08-2020_sd_12-10-', '2020-11-09 02:11:00.000000', NULL, NULL, NULL),
('64eaf273-2a81-4a18-8b3e-b7df43262239', 'MSK-34C', '2020-11-05', 'surat temuan INSPEKTORAT', 'lantai 12', 'hmmm', 'TEMUAN_INSPEKTORAT_urut1.xlsx', '2020-11-09 03:07:15.000000', NULL, NULL, NULL),
('9112a193-9405-40d0-a750-ce7b55691aae', 'ASA4XD-2020', '2020-11-09', 'percobaan keempat', 'lantai 12', '', 'TEMUAN.xlsx', '2020-11-09 02:28:56.000000', NULL, NULL, NULL),
('a915a7f2-b187-41ab-b09b-91c98eb77a6f', 'ASA7XD-2020', '2020-09-22', 'bismillah', 'saya', '...', 'TEMUAN_INSPEKTORAT_urut.xlsx', '2020-11-09 02:44:39.000000', NULL, NULL, NULL),
('aae4ebcd-dfd7-4336-b490-6c573a91021d', 'aaa', '2020-11-09', 'aaa', 'aaa', 'aaa', 'Bukti_Potongan_Pajak_compressed_reduce.pdf', '2020-11-09 03:03:41.000000', NULL, '2020-11-09 03:39:25', NULL),
('c633c43e-7987-4e40-802c-761c982e33b6', 'ASA5XD-2020', '2020-11-08', 'percobaan kelima', 'lantai 12', '', 'Bukti_Potongan_Pajak.pdf', '2020-11-09 02:34:19.000000', NULL, NULL, NULL),
('d5bae6f3-c3db-4092-a1c2-ff447aaaf6d9', 'ASA3XD-2020', '2020-11-08', 'percobaan kembali ketiga', 'aset', '', 'Rangkuman_Permendagri_90_terbaru.xls', '2020-11-09 02:19:36.000000', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keluar`
--
ALTER TABLE `keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masuk`
--
ALTER TABLE `masuk`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
