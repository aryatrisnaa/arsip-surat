-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2020 at 11:02 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surat`
--

-- --------------------------------------------------------

--
-- Table structure for table `masuk`
--

CREATE TABLE `masuk` (
  `id` varchar(100) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `tgl_surat` date NOT NULL,
  `judul` varchar(150) NOT NULL,
  `dari` varchar(200) NOT NULL,
  `keterangan` varchar(250) DEFAULT NULL,
  `nama_file` varchar(50) DEFAULT NULL,
  `created_at` timestamp(6) NULL DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `masuk`
--

INSERT INTO `masuk` (`id`, `no_surat`, `tgl_surat`, `judul`, `dari`, `keterangan`, `nama_file`, `created_at`, `created_by`, `deleted_at`, `deleted_by`) VALUES
('47dcdf8b-c223-4a0e-a246-8b70a5a47a21', 'ASA4XD-2020', '2020-11-10', 'percobaan keempat', 'aset', '', 'NO20.docx', '2020-11-10 04:00:40.000000', NULL, NULL, NULL),
('79b1b328-d230-499b-a534-377ab3c769c9', 'ASA2XD-2020', '2020-09-29', 'rangkuman permendagri', 'mendagri', 'permendagri', 'Rangkuman_Permendagri_90_terbaru.xls', '2020-11-10 03:58:13.000000', NULL, NULL, NULL),
('99300bc9-64a3-467a-8dde-7e59595877d4', 'ASA5XD-2020', '2020-11-05', 'percobaan kembali', 'bpkad', 'test satu dua tiga dicobaa', 'TEMUAN.xlsx', '2020-11-10 04:02:09.000000', NULL, NULL, NULL),
('c0665051-d509-42ec-99c9-718d71e8ffdb', 'ASA3XD-2020', '2020-10-22', 'TEMUAN INSPEKTORAT', 'inspektorat', '', 'TEMUAN_INSPEKTORAT_urut.xlsx', '2020-11-10 03:59:21.000000', NULL, NULL, NULL),
('fb455f9e-2b3e-4865-aeea-e46fb4fe40f5', 'ASA1XD-2020', '2020-11-06', 'bukti potongan pajak', 'lantai 12', '', 'Bukti_Potongan_Pajak_compressed_reduce.pdf', '2020-11-10 03:54:44.000000', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `masuk`
--
ALTER TABLE `masuk`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
