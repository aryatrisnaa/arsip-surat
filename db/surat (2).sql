-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2020 at 03:42 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surat`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id` varchar(25) NOT NULL,
  `user` varchar(25) NOT NULL,
  `pass` varchar(25) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id`, `user`, `pass`, `nama`) VALUES
('001', 'admin', 'admin', 'arya');

-- --------------------------------------------------------

--
-- Table structure for table `dispo`
--

CREATE TABLE `dispo` (
  `id_dispo` varchar(100) NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `kepada` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `tgl_diterima` varchar(11) NOT NULL,
  `no_agenda` varchar(100) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `deleted_at` varchar(20) DEFAULT NULL,
  `deleted_by` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `keluar`
--

CREATE TABLE `keluar` (
  `id` varchar(11) NOT NULL,
  `balasan_surat` varchar(50) DEFAULT NULL,
  `no_surat` varchar(100) NOT NULL,
  `tgl_surat` varchar(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `keluar` varchar(100) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `nama_file` varchar(225) NOT NULL,
  `created_at` varchar(20) NOT NULL DEFAULT 'current_timestamp(6)',
  `created_by` varchar(100) DEFAULT NULL,
  `deleted_at` varchar(20) DEFAULT NULL,
  `deleted_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `masuk`
--

CREATE TABLE `masuk` (
  `id` varchar(100) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `tgl_surat` varchar(11) NOT NULL,
  `judul` varchar(150) NOT NULL,
  `dari` varchar(200) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `nama_file` varchar(225) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `deleted_at` varchar(20) DEFAULT NULL,
  `deleted_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dispo`
--
ALTER TABLE `dispo`
  ADD PRIMARY KEY (`id_dispo`);

--
-- Indexes for table `keluar`
--
ALTER TABLE `keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `masuk`
--
ALTER TABLE `masuk`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
